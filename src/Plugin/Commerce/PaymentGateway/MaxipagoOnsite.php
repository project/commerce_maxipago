<?php

namespace Drupal\commerce_maxipago\Plugin\Commerce\PaymentGateway;

use Drupal\commerce\EntityHelper;
use Drupal\commerce_maxipago\Plugin\Commerce\PaymentGateway\CreditCardMaxipago;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * On-site payment gateways allow the customer to enter credit card details
 * directly on the site. The details might be safely tokenized before they
 * reach the server (Braintree, Stripe, etc) or they might be transmitted
 * directly through the server (PayPal Payments Pro).
 *
 * On-site payment flow:
 * 1) The customer enters checkout.
 * 2) The PaymentInformation checkout pane shows the "add-payment-method"
 *    plugin form, allowing the customer to enter their payment details.
 * 3) On submit, a payment method is created via createPaymentMethod()
 *    and attached to the customer and the order.
 * 4) The customer continues checkout, hits the "payment" checkout step.
 * 5) The PaymentProcess checkout pane calls createPayment(), which charges
 *    the provided payment method and creates a payment.
 *
 * If the payment method could not be charged (for example, because the credit
 * card's daily limit was breached), the customer is redirected back to the
 * checkout step that contains the PaymentInformation checkout pane, to provide
 * a different payment method.
 *
 * @see https://docs.drupalcommerce.org/commerce2/developer-guide/payments/create-payment-gateway/on-site-gateways
 */

/**
 * Provides the On-site payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "maxipago_onsite",
 *   label = "Maxipago (On-site)",
 *   display_label = "Pay with credit card",
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_maxipago\PluginForm\Onsite\PaymentMethodAddForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   requires_billing_information = FALSE,
 * )
 */
class MaxipagoOnsite extends OnsitePaymentGatewayBase implements OnsiteInterface {

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The maxiPago gateway used for making API calls.
   *
   * @var \maxiPago
   */
  protected $api;

  /**
   * Constructs a new Onsite object.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager $payment_type_manager,
    PaymentMethodTypeManager $payment_method_type_manager,
    TimeInterface $time,
    LoggerChannelFactoryInterface $logger_channel_factory,
    MessengerInterface $messenger
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_type_manager,
      $payment_type_manager,
      $payment_method_type_manager,
      $time
    );

    $this->logger = $logger_channel_factory->get('commerce_maxipago');
    $this->messenger = $messenger;
    // Create an instance of the SDK here and assign it to $this->api.
    $this->api = new \maxiPago;
    $this->api->setEnvironment(($this->getMode() == 'test') ? 'TEST' : 'LIVE');
    $id_env = ($this->getMode() == 'test') ? 'merchant_id_test' : 'merchant_id_live';
    $key_env = ($this->getMode() == 'test') ? 'merchant_key_test' : 'merchant_key_live';
    // Do not set credentials if they are not yet set.
    if (!empty($this->configuration[$id_env]) && !empty($this->configuration[$key_env])) {
      $this->api->setCredentials(
        $this->configuration[$id_env],
        $this->configuration[$key_env]
      );
    }
    //$this->api->setDebug(true);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('logger.factory'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'merchant_id_test' => '',
      'merchant_key_test'=> '',
      'merchant_id_live' => '',
      'merchant_key_live'=> '',
      'processor_id' => '',
      'cvv_required' => TRUE,
      'num_installments' => '1',
      'currency' => 'BRL'
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $configuration = $this->getConfiguration();

    $form['merchant_id_test'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#description' => $this->t('Merchant ID for TEST environment.'),
      '#default_value' => $configuration['merchant_id_test'],
      '#size' => 10,
      '#maxlength' => 10,
      '#states' => [
        'visible' => [
          ':input[name="configuration[maxipago_onsite][mode]"]' => [ 'value' => 'test' ]
        ],
        'required' => [
          ':input[name="configuration[maxipago_onsite][mode]"]' => [ 'value' => 'test' ]
        ]
      ]
    ];

    $form['merchant_key_test'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant key'),
      '#description' => $this->t('Merchant key for TEST environment.'),
      '#default_value' => $configuration['merchant_key_test'],
      '#size' => 24,
      '#maxlength' => 24,
      '#states' => [
        'visible' => [
          ':input[name="configuration[maxipago_onsite][mode]"]' => [ 'value' => 'test' ]
        ],
        'required' => [
          ':input[name="configuration[maxipago_onsite][mode]"]' => [ 'value' => 'test' ]
        ]
      ]
    ];

    $form['merchant_id_live'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#description' => $this->t('Merchant ID for LIVE environment.'),
      '#default_value' => $configuration['merchant_id_live'],
      '#size' => 10,
      '#maxlength' => 10,
      '#states' => [
        'visible' => [
          ':input[name="configuration[maxipago_onsite][mode]"]' => [ 'value' => 'live' ]
        ],
        'required' => [
          ':input[name="configuration[maxipago_onsite][mode]"]' => [ 'value' => 'live' ]
        ]
      ]
    ];

    $form['merchant_key_live'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant key'),
      '#description' => $this->t('Merchant key for LIVE environment.'),
      '#default_value' => $configuration['merchant_key_live'],
      '#size' => 24,
      '#maxlength' => 24,
      '#states' => [
        'visible' => [
          ':input[name="configuration[maxipago_onsite][mode]"]' => [ 'value' => 'live' ]
        ],
        'required' => [
          ':input[name="configuration[maxipago_onsite][mode]"]' => [ 'value' => 'live' ]
        ]
      ]
    ];

    $form['processor_id'] = [
      '#type' => 'radios',
      '#title' => $this->t('Payment processor'),
      '#options' => [
        '1' => $this->t('TEST SIMULATOR'),
        '2' => $this->t('Rede'),
        '3' => $this->t('GetNet'),
        '4' => $this->t('Cielo'),
        '5' => $this->t('TEF'),
        '6' => $this->t('Elavon'),
        '8' => $this->t('Chase Paymentech'),
      ],
      '#default_value' => $this->configuration['processor_id'],
      '#required' => TRUE,
    ];

    $form['cvv_required'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Credit card verification number (CVV) is required.'),
      '#description' => $this->t('Please check your payment processor requirements.'),
      '#default_value' => $configuration['cvv_required'],
    ];

    $form['num_installments'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maximum number of installments on the payment'),
      '#description' => $this->t('maxiPago allows split the payment in installments.'),
      '#size' => 3,
      '#maxlength' => 2,
      '#default_value' => $configuration['num_installments'],
      '#required' => TRUE,
    ];

    $form['currency'] = [
      '#type' => 'select',
      '#title' => $this->t('Currency'),
      '#options' => $this->getAvailableCurrencies(),
      '#default_value' => $configuration['currency'],
      '#required' => TRUE,
      '#description' => $this->t('Currency list is defined in the <a href="/admin/commerce/config/currencies">Commerce configuration.</a>'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state
  ) {
    parent::validateConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);

    // Perform validations.
    $key_idx = ($this->getMode() == 'test') ? 'merchant_key_test' : 'merchant_key_live';
    if (strlen($values[$key_idx]) != 24) {
      $form_state->setErrorByName(
        $key_idx,
        $this->t('Merchant key must be 24 characters long.')
      );
    }

    $inst = $values['num_installments'];
    if (!empty($inst) && !is_numeric($inst)) {
      $form_state->setErrorByName(
        'num_installments',
        $this->t('Number of installments must be an integer between 1-99.')
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

      $this->configuration['merchant_id_test'] = $values['merchant_id_test'];
      $this->configuration['merchant_key_test'] = $values['merchant_key_test'];
      $this->configuration['merchant_id_live'] = $values['merchant_id_live'];
      $this->configuration['merchant_key_live'] = $values['merchant_key_live'];
      $this->configuration['processor_id'] = $values['processor_id'];
      $this->configuration['cvv_required'] = $values['cvv_required'];
      $this->configuration['num_installments'] = $values['num_installments'];
      $this->configuration['currency'] = $values['currency'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {

    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    // Perform the create payment request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    // Remember to take into account $capture when performing the request:
    // FALSE if 'Authorize only', TRUE if 'Authorize and capture'
    $configuration = $this->configuration;
    $amount = $payment->getAmount()->getNumber();
    $order = $payment->getOrder();
    $orderNum = $order->getOrderNumber() ? $order->getOrderNumber() : $order->id();

    // Perform the create request to the gateway SDK here
    // Only the 'Sale' method is implemented at this moment, so $capture is ignored

    // The remote ID returned by the request.
    // It will be used when capture payments feature will be implemented.
    $remote_id = $payment_method->getRemoteId();
    if (empty($remote_id)) {
      // If the payment_method failed, do not store the payment method
      $order->set('payment_method', NULL);
      $this->deletePaymentMethod($payment_method);
      throw new HardDeclineException($this->t('The payment has been denied. Please re-enter your payment information.'));
    }
    $next_state = $capture ? 'completed' : 'authorization';

    $payment->setState($next_state);
    $payment->setRemoteId($remote_id);

    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);
    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();

    // Perform the capture request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    $remote_id = $payment->getRemoteId();
    $number = $amount->getNumber();

    $payment->setState('completed');
    $payment->setAmount($amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);
    // Perform the void request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    $remote_id = $payment->getRemoteId();

    $payment->setState('authorization_voided');
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    // Perform the refund request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    $remote_id = $payment->getRemoteId();
    $number = $amount->getNumber();

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

  /**
   * Creates a payment method with the given payment details.
   *
   * In maxiPago, payment methods can be stored using 'quickPago' product.
   * A customer profile must be created before.
   * This feature is NOT IMPLEMENTED at this moment.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *   The payment method.
   * @param array $payment_details
   *   The gateway-specific payment details provided by the payment method form
   *   for on-site gateways, or the incoming request for off-site gateways.
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   *   Thrown when the transaction fails for any reason.
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $required_keys = [
      // The expected keys are payment gateway specific and usually match
      // the PaymentMethodAddForm form elements. They are expected to be valid.
      'number', 'expiration', 'security_code'
    ];
    foreach ($required_keys as $required_key) {
      if (empty($payment_details[$required_key])) {
        throw new \InvalidArgumentException(sprintf('$payment_details must contain the %s key.', $required_key));
      }
    }

    // Perform the create request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    // You might need to do different API requests based on whether the
    // payment method is reusable: $payment_method->isReusable().
    // Non-reusable payment methods usually have an expiration timestamp.
    $configuration = $this->configuration;
    $order = \Drupal::routeMatch()->getParameter('commerce_order');
    if (!$order) {
      throw new HardDeclineException($this->t('The payment method could not be processed'));
    }
    $total = $order->getTotalPrice()->getNumber();
    $amount = number_format($total, 2, '.', '');
    $orderNum = empty($order->getOrderNumber()) ? $order->id() : $order->getOrderNumber();
    $ipAddress = $order->getIpAddress();

    /** @var \maxiPago $maxiPago */
    $maxiPago = $this->api;
    $data = [
      "processorID" => $configuration['processor_id'],
      "referenceNum" => $orderNum,
      "ipAddress" => $ipAddress,
      "chargeTotal" => $amount,
      "number" => $payment_details['number'], // card number
      "expMonth" => $payment_details['expiration']['month'],
      "expYear" => $payment_details['expiration']['year'],
      "cvvNumber" => $payment_details['security_code'],
    ];

    // Installments support
    if (empty($payment_details['num_installments']) || $payment_details['num_installments'] == '1') {
      $maxiPago->creditCardSale($data);
    }
    else {
      $data['action'] = 'new';
      $data['startDate'] = date('Y-m-d');
      $data['period'] = 'monthly';
      $data['frequency'] = '1';
      $data['installments'] = $payment_details['num_installments'];
      $data['failureThreshold'] = '1';
      $maxiPago->createRecurring($data);
    }

    if ($maxiPago->isTransactionResponse() && $maxiPago->getResponseCode() == 0) {
      // The remote ID returned by the request.
      $remote_id = $maxiPago->getAuthCode();
      $this->logger->notice(json_encode($maxiPago->getResult(), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    }
    else {
      $remote_id = '';
      $this->logger->warning(json_encode($maxiPago->getResult(), JSON_PRETTY_PRINT));
    }

    $payment_method->card_type = $payment_details['type'];
    // Only the last 4 numbers are safe to store.
    $payment_method->card_number = substr($payment_details['number'], -4);
    $payment_method->card_exp_month = $payment_details['expiration']['month'];
    $payment_method->card_exp_year = $payment_details['expiration']['year'];
    $expires = CreditCardMaxipago::calculateExpirationTimestamp($payment_details['expiration']['month'], $payment_details['expiration']['year']);

    $payment_method->setRemoteId($remote_id);
    $payment_method->setExpiresTime($expires);
    $payment_method->setReusable(FALSE);

    $payment_method->save();

// TODO: extend PaymentMethod and Payment classes to store the number of installments
// See https://www.drupal.org/project/commerce/issues/3118158#5
// Meanwhile store the number of installments in the order
$order->set('field_num_installments', $payment_details['num_installments']);
$order->save();

  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // Delete the remote record here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    // Delete the local entity.
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function updatePaymentMethod(PaymentMethodInterface $payment_method) {
    // Note: Since requires_billing_information is FALSE, the payment method
    // is not guaranteed to have a billing profile. Confirm that
    // $payment_method->getBillingProfile() is not NULL before trying to use it.
    //
    // Perform the update request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
  }

  /**
   * Returns only the active currencies.
   *
   * We don't want allow to configure a currency which is not active in our
   * site.
   *
   * @return array
   *   The available currencies array values.
   */
  protected function getAvailableCurrencies() {
    // Get the defined currencies for the current shop.
    $currency_storage = $this->entityTypeManager->getStorage('commerce_currency');

    // Load only the enable currencies.
    $currency_ids = $currency_storage
      ->getQuery()
      ->condition('status', TRUE)
      ->execute();

    $available_currencies = [];
    if ($currency_ids) {
      /** @var \Drupal\commerce_price\Entity\CurrencyInterface[] $enabled_currencies */
      $enabled_currencies = $currency_storage->loadMultiple($currency_ids);
      $available_currencies = EntityHelper::extractLabels($enabled_currencies);
    }

    return $available_currencies;
  }
}
