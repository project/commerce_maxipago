README file for Commerce Maxipago

CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Configuration
* How it works
* Troubleshooting
* Maintainers

INTRODUCTION
------------
This project integrates Brazilian maxiPago payment services
(https://www.maxipago.com) into the Drupal Commerce payment and checkout systems.

Payment installments are supported.

IMPORTANT: at this moment, only onsite payments are supported.

  * For a full description of the module, visit the project page:
    https://www.drupal.org/project/commerce_maxipago

  * To submit bug reports and feature suggestions, or to track changes:
    https://drupal.org/project/issues/commerce_maxipago


INSTALLATION
------------
  1) Add the following lines to your project's composer.json,
     section "repositories":

    {
        "type": "package",
        "package": {
          "name": "maxipago/sdk_php",
          "version": "master",
          "source": {
              "url": "https://github.com/maxipago/sdk-php.git",
              "type": "git",
              "reference": "master"
          }
        }
    }

  2) Add the following lines to your project's composer.json,
     section "autoload":

      "autoload": {
        "files": [
            "vendor/maxipago/sdk_php/lib/maxipago/KLogger.php",
            "vendor/maxipago/sdk_php/lib/maxipago/RequestBase.php",
            "vendor/maxipago/sdk_php/lib/maxipago/XmlBuilder.php",
            "vendor/maxipago/sdk_php/lib/maxipago/Request.php",
            "vendor/maxipago/sdk_php/lib/maxipago/ServiceBase.php",
            "vendor/maxipago/sdk_php/lib/maxipago/ResponseBase.php",
            "vendor/maxipago/sdk_php/lib/maxiPago.php"
        ]
      }

  3) Install via composer and the appropriate maxiPago SDK will be downloaded
     to your /vendor folder:

      'composer require drupal/commerce_maxipago'


CONFIGURATION
-------------
  1) Enable the module. The payment method called 'Maxipago onsite' will appear
     in the available payment gateways: admin/commerce/config/payment-gateways.

  2) Edit the gateway and configure the options:

    * 'Mode': select 'Test' or 'Live'.
    * 'Collect billing information': optional for onsite payments.
    * 'Merchant ID/Key': enter the pair for the selected mode. Provided by
       maxiPago when a merchant account is opened.
    * 'Payment processor': select 'TEST SIMULATOR' for test mode, or the
      correct processor for live mode (ussually 'Rede').
    * 'Credit card verification number (CVV) is required' : if checked, up to
      4-digit security code will be required.
    * 'Maximum number of installments': maxiPago allows to split a payment in
      up to 12 (usually monthly) installments. Set here the maximun number
      of installments that customer can select on payment.
    * 'Currency': usually BRL (Brazillian Real)

  3) If you plan to enable installments in the payments, add a Number (integer)
     field to the Order type that is being used
     (admin/commerce/config/order-types/default/edit/fields, replace 'default'
     by the proper order type name). The new integer field *must be* named
     'field_num_installments'. This manual step will be required until a feature
     to store more transaction specific information on Payment entities will be
     available in Commerce (see
     https://www.drupal.org/project/commerce/issues/3118158). Other option would
     be to extend in this module the PaymentMethod and Payment classes,
     to store the number of installments. This is not done at this moment.


HOW IT WORKS
------------

  * General considerations:
    - Shop owner must have a maxiPago Merchant account
    - Customer should have valid credit card accepted by maxiPago.

  * Customer/Checkout workflow:
    This is an On-Site payment method.
    - Credit card form available for "Commerce Maxipago" method selected
      in the payment checkout pane.

  * For test mode, test credit cards and test behavior is explained in:
    https://www.maxipago.com/docs/maxiPago_API_Latest.pdf


TROUBLESHOOTING
---------------
* No troubleshooting pending for now.
* Off-site payment development is pending. Patches welcome.


MAINTAINERS
-----------
Current maintainers:
* Interdruper (interdruper) - https://www.drupal.org/interdruper

This project has been sponsored by:
* Grupo SM publishers - https://www.grupo-sm.com/

