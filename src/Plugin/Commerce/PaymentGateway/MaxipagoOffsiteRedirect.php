<?php

namespace Drupal\commerce_maxipago\Plugin\Commerce\PaymentGateway;


use Drupal\commerce\EntityHelper;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "maxipago_offsite_redirect",
 *   label = "Maxipago (Off-site redirect)",
 *   display_label = "Pay with credit card",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_maxipago\PluginForm\OffsiteRedirect\MaxipagoPaymentOffsiteForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "jcb", "maestro", "mastercard", "visa",
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class MaxipagoOffsiteRedirect extends OffsitePaymentGatewayBase {

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Creates a new MaxipagoOffsiteRedirect object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, LoggerChannelFactoryInterface $logger_channel_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

    $this->logger = $logger_channel_factory->get('commerce_maxipago');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'merchant_id_test' => '',
      'merchant_id_live' => '',
      'processor_id' => '',
      'transaction_type' => 'sale',
      'currency' => 'BRL',
      'payment_language' => 'pt',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $configuration = $this->getConfiguration();

    $form['merchant_id_test'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#description' => $this->t('Merchant ID for TEST environment.'),
      '#default_value' => $configuration['merchant_id_test'],
      '#size' => 10,
      '#maxlength' => 10,
      '#states' => [
        'visible' => [
          ':input[name="configuration[maxipago_onsite][mode]"]' => [ 'value' => 'test' ]
        ],
        'required' => [
          ':input[name="configuration[maxipago_onsite][mode]"]' => [ 'value' => 'test' ]
        ]
      ]
    ];

    $form['merchant_id_live'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#description' => $this->t('Merchant ID for LIVE environment.'),
      '#default_value' => $configuration['merchant_id_live'],
      '#size' => 10,
      '#maxlength' => 10,
      '#states' => [
        'visible' => [
          ':input[name="configuration[maxipago_onsite][mode]"]' => [ 'value' => 'live' ]
        ],
        'required' => [
          ':input[name="configuration[maxipago_onsite][mode]"]' => [ 'value' => 'live' ]
        ]
      ]
    ];

    $form['processor_id'] = [
      '#type' => 'radios',
      '#title' => $this->t('Payment processor'),
      '#options' => [
        '1' => $this->t('TEST SIMULATOR'),
        '2' => $this->t('Rede'),
        '3' => $this->t('GetNet'),
        '4' => $this->t('Cielo'),
        '5' => $this->t('TEF'),
        '6' => $this->t('Elavon'),
        '8' => $this->t('Chase Paymentech'),
      ],
      '#default_value' => $this->configuration['processor_id'],
      '#required' => TRUE,
    ];;

    $form['transaction_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Transaction type'),
      '#options' => [
        'auth' => $this->t('Authorization'),
        'sale' => $this->t('Sale'),
      ],
      '#default_value' => $configuration['transaction_type'],
      '#required' => TRUE,
    ];

    $form['currency'] = [
      '#type' => 'select',
      '#title' => $this->t('Currency'),
      '#options' => $this->getAvailableCurrencies(),
      '#default_value' => $configuration['currency'],
      '#required' => TRUE,
      '#description' => $this->t('Currency list is defined in the <a href="/admin/commerce/config/currencies">Commerce configuration.</a>'),
    ];

    $form['payment_language'] = [
      '#type' => 'select',
      '#title' => $this->t('Language of the payment page'),
      '#options' => [
        'pt' => $this->t('Portuguese'),
        'en' => $this->t('English'),
        'es' => $this->t('Spanish'),
      ],
      '#default_value' => $configuration['payment_language'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state
  ) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

      $this->configuration['merchant_id_test'] = $values['merchant_id_test'];
      $this->configuration['merchant_id_live'] = $values['merchant_id_live'];
      $this->configuration['processor_id'] = $values['processor_id'];
      $this->configuration['transaction_type'] = $values['transaction_type'];
      $this->configuration['currency'] = $values['currency'];
      $this->configuration['payment_language'] = $values['payment_language'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    // @todo Add examples of request validation.
    // Note: Since requires_billing_information is FALSE, the order is
    // not guaranteed to have a billing profile. Confirm that
    // $order->getBillingProfile() is not NULL before trying to use it.
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $payment_storage->create([
      'state' => 'completed',
      'amount' => $order->getBalance(),
      'payment_gateway' => $this->parentEntity->id(),
      'order_id' => $order->id(),
      'remote_id' => $request->query->get('txn_id'),
      'remote_state' => $request->query->get('payment_status'),
    ]);
    $payment->save();
  }

  /**
   * Returns only the active currencies.
   *
   * We don't want allow to configure a currency which is not active in our
   * site.
   *
   * @return array
   *   The available currencies array values.
   */
  protected function getAvailableCurrencies() {
    // Get the defined currencies for the current shop.
    $currency_storage = $this->entityTypeManager->getStorage('commerce_currency');

    // Load only the enable currencies.
    $currency_ids = $currency_storage
      ->getQuery()
      ->condition('status', TRUE)
      ->execute();

    $available_currencies = [];
    if ($currency_ids) {
      /** @var \Drupal\commerce_price\Entity\CurrencyInterface[] $enabled_currencies */
      $enabled_currencies = $currency_storage->loadMultiple($currency_ids);
      $available_currencies = EntityHelper::extractLabels($enabled_currencies);
    }

    return $available_currencies;
  }

}
