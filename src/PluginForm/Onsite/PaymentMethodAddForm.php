<?php

namespace Drupal\commerce_maxipago\PluginForm\Onsite;

use Drupal\commerce_maxipago\Plugin\Commerce\PaymentGateway\CreditCardMaxipago;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm as BasePaymentMethodAddForm;
use Drupal\Core\Form\FormStateInterface;

class PaymentMethodAddForm extends BasePaymentMethodAddForm {

  /**
   * Builds the credit card form.
   *
   * @param array $element
   *   The target element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   *
   * @return array
   *   The built credit card form.
   */
  public function buildCreditCardForm(array $element, FormStateInterface $form_state) {
    $element = parent::buildCreditCardForm($element, $form_state);
    // Get whether cvv is required from the payment gateway's configuration.
    $payment_gateway = $this->entity->getPaymentGateway();
    $payment_gateway_config = $payment_gateway->getPluginConfiguration();
    $element['security_code']['#required'] = empty($payment_gateway_config['cvv_required']) ? FALSE : TRUE;

    // Get whether installments are configured in the gateway
    if ($payment_gateway_config['num_installments'] > 1) {
      $options = [];
      $element['num_installments'] = [
        '#type' => 'select',
        '#title' => $this->t('Number of installments'),
        '#options' => range(1, $payment_gateway_config['num_installments'], 1),
        '#required' => FALSE,
        '#description' => $this->t('Number of installments for your payment.'),
      ];
    }

    return $element;
  }

  /**
   * Validates the credit card form.
   *
   * @param array $element
   *   The credit card form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   */
  protected function validateCreditCardForm(array &$element, FormStateInterface $form_state) {

    $values = $form_state->getValue($element['#parents']);
    $card_type = CreditCardMaxipago::detectType($values['number']);
    if (!$card_type) {
      $form_state->setError($element['number'], $this->t('You have entered a credit card number of an unsupported card type.'));
      return;
    }

    // Validate that the card type is one of the accepted by the payment
    // gateway.
    $accepted_card_types = CreditCardMaxipago::getTypeLabels();
    if (!isset($accepted_card_types[$card_type->getId()])) {
      $form_state->setError(
        $element['number'],
        $this->t(
          'The %card_type card type is not currently accepted.',
          ['%card_type' => $card_type->getLabel(),]
        )
      );
    }

    // Validate card number
    if (!CreditCardMaxipago::validateNumber($values['number'], $card_type)) {
      $form_state->setError($element['number'], $this->t('You have entered an invalid credit card number.'));
    }

    // Validate expiration date
    if (!CreditCardMaxipago::validateExpirationDate($values['expiration']['month'], $values['expiration']['year'])) {
      $form_state->setError($element['expiration']['month'], t('You have entered an expired credit card.'));
    }

    // CVV should be validated only if required.
    $payment_gateway = $this->entity->getPaymentGateway();
    $payment_gateway_config = $payment_gateway->getPluginConfiguration();
    $test_mode = $payment_gateway_config['mode'] == 'test';
    $cvv_required = empty($payment_gateway_config['cvv_required']) ? FALSE : TRUE;
    if (!$test_mode && $cvv_required && !CreditCardMaxipago::validateSecurityCode($values['security_code'], $card_type)) {
      $form_state->setError($element['security_code'], $this->t('You have entered an invalid CVV.'));
    }

    // Persist the detected card type.
    $form_state->setValueForElement($element['type'], $card_type->getId());
  }

  /**
   * Handles the submission of the credit card form.
   *
   * @param array $element
   *   The credit card form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   */
  protected function submitCreditCardForm(array $element, FormStateInterface $form_state) {
    $values = $form_state->getValue($element['#parents']);
    $this->entity->card_type = $values['type'];
    $this->entity->card_number = substr($values['number'], -4);
    $this->entity->card_exp_month = $values['expiration']['month'];
    $this->entity->card_exp_year = $values['expiration']['year'];
  }
}
